# C++ Progress Bar

A simple progress bar for the terminal in Unix-like OS.

Just `#include` the `inlcude/mabl3/ProgressBar.h` in your project and get going.

### Usage

Initialize a progress bar by creating a `ProgressBar` object with the total number
of steps you want to monitor.

```
auto total = 1000;
auto pb = mabl3::ProgressBar(total);
```

This will print `[    ] 0%` to a new line in the terminal.

#### Updating the Bar

There are four ways:
* Increment by one via `++pb;` or `pb++;`
* Increment by a certain number of steps `n` via `pb += n;`
* Set the progress to a certain number `p` via `pb.update(p);`

The last two methods also allow to decrease the progress by passing
negative integers.

### Special Usage

You can silence the bar, e.g. for productive use of your project, 
by setting the `quiet` flag in the constructor

`auto pb = mabl3::ProgressBar(total, true);`

You can manually set the width of the bar (_only_ the bar, i.e. the `[===   ]` part)
with

```
auto desiredWidth = 20;
auto pb = mabl3::ProgressBar(total, false, desiredWidth);
```

### Caveats

While this is a small and simple library, there are some things to
consider to ensure it's working as expected.

There are no measures to ensure proper behaviour with invalid input, i.e.
you _can_ increase the bar above 100% and it is your responsibility 
to make sure this does not happen. The bar cannot represent negative
progress so don't try to do this.

The progress bar is printed immediately and directly to `std::cout`,
make sure that, while the bar is still "active", there is no other
output to `std::cout`. Once you've reached 100%, the bar will print
a `std::endl` and you can savely output things again. If you abort
before that, don't forget to print the newline yourself.
