/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * C++ Progress Bar
 *   https://gitlab.com/mabl3/cpp-progress-bar
 *   Version 0.1.0
 *
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Matthis Ebel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <sys/ioctl.h>  // For ioctl, TIOCGWINSZ
#include <unistd.h>     // For STDOUT_FILENO

namespace mabl3 {

//! Simple progress bar for the terminal
class ProgressBar {
public:
    //! c'tor
    /*! \param total Total number of elements (i.e. what is the 100% point)
     * \param quiet Do not print anything to stdout if true
     * \param barwidth Width of the bar (without annotation) in characters,
     *   leave at 0 to use maximal width that fits in a single terminal line */
    ProgressBar(size_t total, bool quiet = false, size_t barwidth = 0) :
        barw_{barwidth}, current_{0},
        step_{static_cast<size_t>(std::ceil(static_cast<double>(total) / 100.))},
        next_{step_}, quiet_{quiet},  total_{total} {
        // if barw_ is 0 (default), determine fitting barwidth
        if (!quiet_ && barw_ == 0) {
            struct winsize wsize;
            ioctl(STDOUT_FILENO, TIOCGWINSZ, &wsize);
            if (wsize.ws_col < 8) { // not enough space for a bar (min: `[=] 100%`)
                quiet_ = true;
            } else {
                // 7 chars for `[] 100%`
                barw_ = wsize.ws_col - 7;
            }
        }
        if (!quiet_) {
            std::cout << "Bar -- total: " << total_ << ", step: " << step_ << std::endl;
            std::cout << "[";
            for (size_t i = 0; i < barw_; ++i) {
                std::cout << " ";
            }
            std::cout << "] 0%\r";
            std::cout.flush();
        }
    }
    //! If not quiet, print a newline
    void finish() const {
        if (!quiet_) { std::cout << std::endl; }
    }
    //! Increase progress count by one (prefix increment)
    ProgressBar & operator++() {
        current_++;
        update();
        return *this;
    }
    //! Increase progress count by one (postfix increment)
    ProgressBar operator++(int) {
        auto tmp = *this;
        ++*this;
        return tmp;
    }
    //! Increase progress count by \c progress (decrease with nedative value)
    ProgressBar & operator+=(int progress) {
        current_ += progress;
        update();
        return *this;
    }
    //! Return if \c quiet_
    bool quiet() const { return quiet_; }
    //! Return the required progress to visually increase the bar by one character
    size_t step() const { return step_; }
    //! Set the progress to \c idx
    void update(size_t idx) {
        current_ = idx;
        update();
    }

private:
    size_t barw_;       // bar width
    size_t current_;    // current progress
    size_t step_;       // step size
    size_t next_;       // next required progress to increase the bar visually
    bool quiet_;        // do not output anything if true
    size_t total_;      // expected total progress

    void printBar(size_t percent) {
        if (!quiet_) {
            std::cout << "[";
            auto bar = static_cast<size_t>(std::floor(static_cast<double>(barw_ * percent) / 100.));
            for (size_t i = 0; i < barw_; ++i) {
                if (i <= bar) std::cout << "=";
                else std::cout << " ";
            }
            std::cout << "] " << percent << "%\r";
            std::cout.flush();
        }
    }
    void update() {
        if (current_ >= next_) {
            auto progress = static_cast<double>(current_) / static_cast<double>(total_);
            auto percent = static_cast<size_t>(std::floor(progress * 100.));
            next_ += step_;
            if (next_ > total_) { next_ = total_; }
            printBar(percent);
            if (current_ == total_ && (!quiet_)) { std::cout << std::endl; }
        }
    }
};

} // NAMESPACE mabl3

#endif // PROGRESSBAR_H
