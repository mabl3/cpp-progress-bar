#include <chrono>
#include <thread>

#include <mabl3/ProgressBar.h>

int main() {
    auto total = 10000;

    // initialize progress bar
    auto pb = mabl3::ProgressBar(total);

    // different ways to update progress bar
    for (auto i = 0; i < total-1; ++i) {
        if (i < i/4) {
            ++pb;   // prefix increment
        } else if (i < i/2) {
            pb++;   // postfix increment
        } else if (i < 3*i/4) {
            pb += 1;    // increment by certain number
        } else {
            pb.update(i+1); // set progress to a certain number
        }
        // just so you can see the bar growing
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    pb.finish(); // incomplete pb, print newline

    pb = mabl3::ProgressBar(total);
    for (auto i = 0; i < total; ++i) {
        ++pb;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    } // should print newline itself when at 100%
}
